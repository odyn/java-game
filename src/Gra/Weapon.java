package Gra;

public class Weapon implements IGameObject {

	int x;
	int y;
	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public void setX(int v) {
		this.x = v;
	}

	@Override
	public void setY(int v) {
		this.y = v;
	}

	@Override
	public void display() {
		System.out.println("Player");
	}

	@Override
	public void move(int dx, int dy) {
		this.x += dx;
		this.y += dy;
	}
}
