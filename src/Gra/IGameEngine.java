package Gra;

public interface IGameEngine {
	void start();
	void stop();
	void display(IGameObject gameObj);
}
