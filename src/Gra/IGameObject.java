package Gra;

public interface IGameObject {
	int getX();
	int getY();
	void setX(int v);
	void setY(int v);
	void display();
	void move(int dx, int dy);
}
