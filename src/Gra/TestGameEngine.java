package Gra;

public class TestGameEngine implements IGameEngine {

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display(IGameObject gameObj) {
		gameObj.display();
		
	}
	
	public static void main(String [ ] args)
	{
	   
		System.out.print("Marek Ormaniec\n");
	   
		IGameObject p1 = new Player();
		IGameObject sword = new Sword();
		IGameEngine game = new TestGameEngine();
		
		game.display(p1);
		game.display(sword);
		
		p1.setX(10);
		p1.setY(15);
		p1.move(3, 4);	
		
		
	}

}
